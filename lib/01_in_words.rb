

class Fixnum

  $string = ""

  $ones = [" zero", " one ", " two ", " three ", " four ", " five ", " six ", " seven ", " eight ", " nine "]
  $tens = { 20 => " twenty ", 30 => " thirty ", 40 => " forty ",  50 => " fifty ", 60 => " sixty ", 70 => " seventy ", 80 => " eighty ", 90 => " ninety ", 10 => " ten ", 11 => " eleven ", 12 => " twelve ", 13 => " thirteen ", 14 => " fourteen ", 15 => " fifteen ", 16 => " sixteen ", 17 => " seventeen ", 18 => " eighteen ", 19 => " nineteen "}

  def round(exp = 0)
    multiplier = 10 ** -exp
    ((self.to_f * multiplier).floor/multiplier).to_i
  end

  def size(number)
    number.to_s.split("").count
  end

  def populate_string(num, count, modulo, string)
    magnitude = ((num).round(count)/modulo)
    if num >= modulo
      if size(magnitude) == 1
        ones(magnitude)
      elsif size(magnitude) == 2
        tens(magnitude)
      else
        hundreds(magnitude)
      end
      $string << string
    end
  end

  def ones(num=self)
     $string << $ones[num % 10] unless num % 10 == 0
  end

  def tens(num=self)
    ten = (num % 100).round(1)

    if ten > 10
      $string << $tens[ten]
      ones(num % 10)
    elsif ten == 10
      $string << $tens[num]
    else
      ones
    end
  end

  def hundreds(num=self)
    populate_string(num, 2, 100, "hundred")
    tens(num % 100)
  end

  def thousands(num=self)
    populate_string(num, 3, 1_000, "thousand")
    hundreds(num % 1_000)
  end

  def millions(num=self)
    populate_string(num, 4, 1_000_000, "million")
    thousands(num % 1_000_000)
  end

  def billions(num=self)
    populate_string(num, 5, 1_000_000_000, "billion")
    millions(num % 1_000_000_000)
  end
  #
  def trillions(num=self)
    populate_string(num, 6, 1_000_000_000_000, "trillion")
    billions(num % 1_000_000_000_000)
  end

  def in_words
    $string = ""
    return "zero" if self == 0

    self.trillions
    return $string.split.join(" ")
  end

end



p 1_000_000_000_312.in_words
